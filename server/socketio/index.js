const { Server } = require("socket.io");

exports.init = (http_server)=>{
    const io = new Server(http_server,{
        allowEIO3: true,
    });
    io.on('connection', async (socket) => {
        // console.log('a user connected');

        let all =await io.of("/").allSockets();
        io.emit('total', all.size);//
        console.log('[' + (new Date()).toUTCString() + '] game connecting,users:',all.size);

        socket.on('total', async (msg) => {
            let all =await io.of("/").allSockets();
            io.emit('total', all.size);//
        });

        socket.on('message', (data)=>{
            io.emit('message', data);
        });

        //该函数应该加锁
        socket.on('join',async (room)=> {
            socket.join(room);
            console.log('join room socket :',socket.id);
            let oflist = io.of("/")
            let myRoom =await io.of("/").in(room).allSockets();

            var users = myRoom.size;

            console.log('the number of user in room is: ' +room+"::"+ users);
            socket.emit('joined', room, socket.id);
           
        });

        socket.on('leave', async (room)=> {
            let users = await io.of("/").in(room).allSockets();

            console.log('the number of user in room is: ' + (users-1));

            socket.leave(room);
            socket.to(room).emit('bye', room, socket.id)
            socket.emit('leaved', room, socket.id);
        });
        //Unity Server
        //This funciton is needed to let some time pass by between conversation and closing. This is only for demo purpose.
        function sleep(ms) {
            return new Promise((resolve) => {
                setTimeout(resolve, ms);
            });
        }

        socket.on('KnockKnock', (data) => {
            console.log('[' + (new Date()).toUTCString() + '] game knocking... Answering "Who\'s there?"...');
            socket.emit('WhosThere');
        });

        socket.on('ItsMe', async (data) => {
            console.log('[' + (new Date()).toUTCString() + '] received game introduction. Welcoming the guest...');
            socket.emit('Welcome', 'Hi customer using unity' + data.version + ', this is the backend microservice. Thanks for buying our asset. (No data is stored on our server)');
            socket.emit('TechData', {
                podName: 'Local Test-Server',
                timestamp: (new Date()).toUTCString()
            });
        });

        socket.on('SendNumbers', async (data) => {
            console.log('[' + (new Date()).toUTCString() + '] Client is asking for random number array');
            socket.emit('RandomNumbers', [ Math.ceil((Math.random() * 100)), Math.ceil((Math.random() * 100)), Math.ceil((Math.random() * 100)) ]);
        });

        socket.on('Goodbye', async (data) => {
            console.log('[' + (new Date()).toUTCString() + '] Client said "' + data + '" - The server will disconnect the client in five seconds. You can now abort the process (and restart it afterwards) to see an auto reconnect attempt.');
            await sleep(5000); //This is only for demo purpose.
            socket.disconnect(true);
        });

        socket.on('disconnect', async (data) => {
            let users = await io.of("/").allSockets()
            io.emit('total', users.size);//
            console.log('[' + (new Date()).toUTCString() + '] Bye, client ' + socket.id,',user number:'+users.size);
        });



        socket.on('PING', async (data) => {
            // console.log('[' + (new Date()).toUTCString() + '] incoming PING #' + data + ' from ' + socket.id + ' answering PONG with some jitter...');
            await sleep(Math.random() * 2000);
            socket.emit('PONG', data);
        });
    });
}
