const express = require('express');
const app = express();
const http = require('http');

const server = http.createServer(app);
const socketIo = require('./socketio/index')
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/html/index.html');
});


app.get('/test', function(req, res, next) {
    res.send('respond with a resource');
});
socketIo.init(server)
server.listen(3000, () => {
    console.log('listening on *:3000');
});
